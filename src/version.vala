const string VERSION = "11.6.1";

namespace Pamac {
	public string get_version () {
		return VERSION;
	}
}
